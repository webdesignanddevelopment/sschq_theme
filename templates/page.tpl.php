<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>

<div id="page">
<div id="top-bar" class="full-width visuallyhidden">
  <div class="page-info">
    <h2 class="visuallyhidden">Site Information and assistance</h2>
    <?php /* We can't use anchors in Drupal menus, so this menu is manual */ ?>
    <ul class="page-info-menu">
      <li><a href="<?php print url('node/896');?>">Get assistance with this site</a></li>
      <li class="mobile-only"><a href="#search-block-form">Jump to Search</a></li>
      <li><a href="#navigation">Jump to navigation</a></li>
      <li><a href="#content"><span class="visuallyhidden">Jump to</span> content</a></li>
      <li><a href="#footer"><span class="visuallyhidden">Jump to</span> footer</a></li>
    </ul>
  </div>

  <!-- If anyone has extra stuff to display at this region -->
  <?php if ($page['top_bar_extra']): ?>
    <div id="top-bar-extra"><?php print render($page['top_bar_extra']); ?></div>
  <?php endif; ?>

</div>

<div class="print-only">
  <img src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_400_print.png" srcset="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_400_print.png 1x, /<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_800_print.png 2x" alt = "Australian Government" />
</div>

<div id="page-wrapper" class="centered w1050 layout2 clearfix">
  <div id="side-bar" class="w30percent content-left">
    <div id="header" class="clearfix topheight">
      <div id="header-content">
        <!-- LOGO -->
        <div id="logo">
          <a class="crest" href="/" title="Go to the <?php print $site_name; ?> home page"><img src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_200.png" srcset="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_200.png 1x, /<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_crest_400.png 2x"  alt="Australian Government | Go to the <?php print $site_name; ?> home page" /></a>
        </div>

        <?php if ($page['header_extra']): ?>
          <div id="header-extra"><?php print render($page['header_extra']); ?></div>
        <?php endif; ?>
      </div>
    </div>
    <?php if ($page['side_bar']): ?>
      <div id="side-bar-extra"><?php print render($page['side_bar']); ?></div>
    <?php endif; ?>
  </div>

  <div id="content" class="w70percent content-right">
    <!-- Render out the page title if not front page -->

    <?php
      global $user;
      $current_path = drupal_get_path_alias(current_path());

      if(user_is_logged_in()) {
        $roles = $user->roles;
        $username = $user->name;
        unset($roles[2]);
        $role = implode('', $roles);
        $string = 'Customer-';
        $pos = strpos($role, $string);
        $pattern = "/Customer-/i";
        $replacement = '';

        if($pos !== false) {
          $role = preg_replace($pattern, $replacement, $role);
        }
      }

    ?>
    <div id="title-bg">
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>

        <h1 class="title topheight" id="page-title" property="dc:title">
          <span class="contact-480">
            Get in touch with the SSC,
            <a href="/contact">Contact&nbsp;us</a>
          </span>
          <?php if (arg(0) == 'user' && arg(1) == 'register') : ?>
            Create a SSC Account
          <?php elseif (arg(0) == 'user' && arg(1) == 'password') : ?>
            Retrieve lost password
          <?php elseif (arg(0) == 'user' &&  arg(1) == 'login') : ?>
            SSC Customer Login
          <?php elseif (arg(0) == 'user' &&  $user->uid === 0) : ?>
            SSC Customer Login
          <?php elseif (arg(0) == 'user') : ?>
            SSC Customer Login
          <?php elseif (($current_path === "dashboard") && ($pos !== false)) : ?>
          <?php print $role . " Dashboard"; ?>
          <?php else : ?>
            <?php print $title ?>
          <?php endif ; ?>

        </h1>
        <?php endif; ?>
      <?php print render($title_suffix); ?>
    </div>
      <div class="contents clearfix">
        <div id="indent-main">
        <!-- Render out the Breadcrumb if not front page -->
        <?php if (!$is_front) : ?>
          <?php if ($breadcrumb): ?>
            <div id="breadcrumb" class="font-12px"><?php print render($breadcrumb); ?></div>
          <?php endif; ?>
        <?php endif; ?>
        <?php if($current_path === "dashboard") : ?>
          <h2>Welcome <?php echo $username; ?></h2>
        <?php endif; ?>
      </div>
      <?php if ($page['highlighted']): ?>
        <div id="highlighted"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>

      <?php print $messages; ?>

      <?php if ($tabs): ?>
        <div class="tabs"><?php print render($tabs); ?></div>
      <?php endif; ?>

      <?php print render($page['help']); ?>

      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>
  </div>
</div>

<?php if (isset($page['tabbed']) && !empty($page['tabbed'])): ?>
<!-- Tabbed section -->
  <div id="tabbed">
    <div class="tab-container">
      <?php print render($page['tabbed']); ?>
    </div>
  </div>
<?php endif; ?>

<!-- FOOTER REGION -->
<footer id="footer" role="contentinfo" class="font-12px clearfix section">
  <?php if (isset($page['cta'])): ?>
    <div id="ftop" class="clearfix">
    <!-- Call to action -->
      <div id="cta"><?php print render($page['cta']); ?></div>
    </div>
  <?php endif; ?>

  <?php if (isset($page['footer'])): ?>
    <div id="fmid" class="clearfix">
      <div class="footer-nav-region centered w1050">
        <?php print render($page['footer']); ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="print-only centred">
    <img src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/images/ssc_logo_200_colour_print.png" alt = "Shared Service Centre Logo" />
  </div>

  <?php if (isset($page['baseline'])): ?>
    <div id="fbtm" class="clearfix">
      <!-- Baseline -->
      <div id="baseline" class="centered w1050">
        <?php print render($page['baseline']); ?>
      </div>
    </div>
  <?php endif; ?>
    <?php if (isset($page['last_modify'])): ?>
    <div id="last_modify" class="clearfix">
      <?php print render($page['last_modify']); ?>
    </div>
  <?php endif; ?>
</footer>
