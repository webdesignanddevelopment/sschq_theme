<?php
 /**
 * @file field__field_file.tpl.php
 * This file is intended to format file fields as part of the file information field collection
 *
 * Available variables:
 *   $file_path = the url to the file itself
 *   $file_ext = the file's extension in lowercase
 *   $file_size = the file's size with appropriate text included (eg 4MB, 18TB)
 *
 * Standard variables used:
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 *
 */


/* Set up a link around the chart ID *if* the user is allowed to edit nodes.
 * Makes for quick editing of charts!! Yay for efficiency!
*/

?>
<?php if ($show_link):?>
  <a class="<?php print $classes; ?>" href="<?php print $link_path;?>">
<?php else : ?>
  <div class="<?php print $classes; ?>">
<?php endif;?>
<small>
<?php print $items[0]['#markup']; ?>
</small>
<?php if ($show_link):?>
  </a>
<?php else:?>
</div>
<?php endif;?>
