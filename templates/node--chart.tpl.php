<?php

if($node->field_chart_title['und'][0]['value']) {
	$chart_title = $node->field_chart_title['und'][0]['value'];
}

if(!empty($node->field_chart_description['und'][0]['value'])) {
	$chart_description = $node->field_chart_description['und'][0]['value'];
}

if($node->field_chart_id['und'][0]['value']) {
	$chart_id = $node->field_chart_id['und'][0]['value'];
}

if($node->field_ref_servicelevels['und'][0]['value']) {
	$ref_servicelevels = $node->field_ref_servicelevels['und'][0]['value'];
}

if($node->field_legend_names) {
	$legend_names = $node->field_legend_names['und'];
	foreach ($legend_names as $legend_name) {
		$legendname .= $legend_name['value'] . ",";
	}
	$legendnametpl = "names: {" . $legendname . "},"; 
}

if($node->field_month_axis['und'][0]['value']) {
	$month_axis = $node->field_month_axis['und'][0]['value'];
}

if($node->field_months['und'][0]['value']) {
	$months = explode(',', $node->field_months['und'][0]['value']);
	$months = implode("','", $months);
	$months = "['" . $months . "']";
}

if(!empty($node->field_chart_labels['und'][0]['value'])) {
	$chart_label = $node->field_chart_labels['und'][0]['value'];
}

if($node->field_target['und'][0]['value']) {
	$target = $node->field_target['und'][0]['value'];
}

if($node->field_charts_data['und'][0]['value']) {
	$charts_data = $node->field_charts_data['und'][0]['value'];
	$chardata = explode(",", $charts_data);
	$numOfItems = count($chardata);

	if(!empty($target)) {
		for ($i = 0; $i < $numOfItems; $i++) {
			$targetString .= $target . ", ";
		}
	}

	$charts_data_string = "['Data2', " . $charts_data . "]";
	$target_data_string = "['Data1', " . $targetString . "]";

}

if(empty($node->field_colour_theme['und'][0]['value'])) {
	$node->field_colour_theme['und'][0]['value'] = t("color: function (color, d) { var target = !target; 
		               if(typeof d.id !== 'undefined' && d.id === 'Data2') {   
                       cValue = 1 - d.value/target;
                       if (cValue >= 0.2 ) {
                          color = d3.rgb('#d83948');
                       }
                       else if (cValue >= 0.1) {
                          color = d3.rgb('#f97433');
                       }
                       }    
                       return d.id && d.id === 'Data2' ? d3.rgb(color) : color; },", array('!target' => $target));
	$colour_theme = $node->field_colour_theme['und'][0]['value'];
}
else {
	$colour_theme = $node->field_colour_theme['und'][0]['value'];
}

if($node->field_colours['und'][0]['value']) {
	$chart_colours = $node->field_colours['und'][0]['value'];
	$chartcoltpl = "colors: {" . $chart_colours . "}, ";
}

if($node->field_types) {
	$chart_types = $node->field_types['und'];
	foreach ($chart_types as $chart_type) {
		$charttype .= $chart_type['value'] . ",";
	}
	$charttpl = "types: {" . $charttype . "},";
}

if($node->field_axes['und'][0]['value']) {
	$chart_axes = $node->field_axes['und'][0]['value'];
	$chartaxestpl = "axes: {" . $chart_axes . "},";
}

if(!empty($node->field_configura_axis['und'][0]['value'])) {
	$chart_axis = $node->field_configura_axis['und'][0]['value'];
}

$search = ".";
$replacement = "_";
$chartString  = $chart_id . $ref_servicelevels;
$chartDisplayId = str_replace($search, $replacement, $chartString);
$chartDisplayTpl = "<div id='" . $chartDisplayId . "'></div>";

$bindTo   = "bindto: '#" . $chartDisplayId . "',";

$realdata = "data: {" . $month_axis . ",
			  	columns: [" . $months .",". $target_data_string . "," . $charts_data_string . "]," .
			  	$charttpl . 
			  	$legendnametpl . 
			  	$chart_label . 
			  	$chartaxestpl . 
			  	$chartcoltpl . 
			  	$colour_theme .
			 "},";

$tpl = $bindTo . $realdata . $chart_axis;

$jstpl = "var chart = c3.generate({" . $tpl . "});";

$json_template = "<script type='text/javascript'>" . $jstpl . "</script>"; 

if(empty($node->body['und'][0]['value'])) {
	$chartCodes = $chartDisplayTpl . $json_template;
}
else {
	$chartCodes = $node->body['und'][0]['value'];
}

/* TODO  Make a description field render underneath the chart */

$final_html_template = "<div class='chart'>
							<h4>".$chart_title."</h4>
							<p class='description'>" . $chart_description . "</p>"
							. $chartCodes .
							"<!-- description -->
						</div>";

print render($final_html_template);
print render($content['field_chart_id']);
