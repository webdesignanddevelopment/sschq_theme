<?php

/* This template is used to render People in Views. */

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
/* quick test so we don't waste time */
if ($row->node_type !== 'person') {return;}

 /* define some variables with defaults */
$link = url('node/'.$row->nid);
$nid = $row->nid;
$name = $row->node_title;
$initials = get_initials($name);
$job_title = false;
$phone =  '02 6123 4444 (temp)';
$location = '<p>60 Marcus Clarke St<br />Canberra ACT, Australia (temp)</p>';
$email = 'info@ssc.gov.au';
$person_type = 'unknown';
$person_color = "#333";
$profile_photo = false;
$image_style = 'profile_image';
$image = false; /* this is used in the final template */
$description = false;

/* Referring node */
$referring_node = menu_get_object();
$rid = isset($referring_node->nid)? $referring_node->nid : 'view';

/* Go and fill these with good values if they exist */
if (isset($row->field_field_job_title) && isset($row->field_field_job_title[0])){
  $job_title = $row->field_field_job_title[0]['raw']['safe_value'];
}
if (isset($row->field_field_phone) && isset($row->field_field_phone[0])){
  $phone = $row->field_field_phone[0]['raw']['safe_value'];
}
if (isset($row->field_field_location) && isset($row->field_field_location[0])){
  $location = $row->field_field_location[0]['rendered']['#markup'];
}
if (isset($row->field_field_email) && isset($row->field_field_email[0])){
  $email = $row->field_field_email[0]['raw']['safe_value'];
  $ob_email = hide_email($email);
} else {
  $ob_email = hide_email($email);
}
if (isset($row->field_field_person_type) && isset($row->field_field_person_type[0])){
  $person_type = $row->field_field_person_type[0]['raw']['value'];
}
if (isset($row->field_field_profile_photo) && isset($row->field_field_profile_photo[0])){
  $profile_photo = $row->field_field_profile_photo;
}
if (isset($row->field_field_short_description) && isset($row->field_field_short_description[0])){
  $description = $row->field_field_short_description[0]['rendered']['#markup'];;
}

if ($profile_photo){
  /* theme up the image */
  $img_item = array(
     'item' => array(
       'alt' => addslashes($name),
       'title' => addslashes($name),
       'uri' => $profile_photo[0]['rendered']['#item']["uri"],
       'attributes' => array('class' => 'profile-photo circle')
     ),
    'image_style' => $image_style
  );
  $image = theme_image_formatter($img_item);
} else {
  /* create some html to hold their initials */
  $image = "<div class='profile-photo has-initials '><div class='circle $person_type'><span class='initials'>$initials</span></div></div>";
}

/* Do some rendering! */
$output = "
<div class='person-profile person-$row->nid clearfix'>
  <div class='profile-intro'>
    <a href='$link'>
    $image
    <div class='profile-name'>
        $name";
if ($job_title){
  $output .= "<span class='profile-title'>$job_title</span>";
}
$output .= "
    </div><!-- / .profile-name -->
    </a>
  </div><!-- / .profile-intro -->
  <div class='person-details'>
    <div class='phone'>
      <strong>Phone</strong>
      $phone
    </div>
    <div class='location'>
    <strong>Location</strong>
      $location
    </div>
    <div class='email'>
      <strong>Contact $name</strong>
      <a href='/node/137?pid=$nid&rid=$rid' class='button message'>Send $name a message</a>
    </div>
  </div>";
if ($description){
  $output .= "
    <div class='profile-description'>
      $description
    </div>";
}

//dpm($output);

print $output;

