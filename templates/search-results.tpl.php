<?php
// $Id: search-results.tpl.php,v 1.7 2010/08/18 18:40:50 dries Exp $

/**
 * @file
 * Default theme implementation for displaying search results.
 *
 * This template collects each invocation of theme_search_result(). This and
 * the child template are dependent to one another sharing the markup for
 * definition lists.
 *
 * Note that modules may implement their own search type and theme function
 * completely bypassing this template.
 *
 * Available variables:
 * - $search_results: All results as it is rendered through
 *   search-result.tpl.php
 * - $module: The machine-readable name of the module (tab) being searched, such
 *   as "node" or "user".
 *
 *
 * @see template_preprocess_search_results()
 */
?>
<?php if ($search_results): ?>
  <h2><?php print $results_title;?></h2>
  <?php if ($search_term): ?>
    <p>Based on your search term<?php print (stristr($search_term, ' ')?'s': '');?> <em>(<?php print $search_term; ?>)</em> you may be interested in:</p>
  <?php endif; ?>
  <ol class="search-results <?php print $module; ?>-results">
    <?php print $search_results; ?>
  </ol>
  <?php print $pager; ?>
<?php else :
  /* If you are using core search, this will kick in, otherwise check theme_apachesolr_hook_noresults(); */
  ?>
  <h2>No results found</h2>
  <?php if ($search_term): ?>
    <p>There were no results that contained your term<?php print (stristr($search_term, ' ')?'s': '');?> <em><?php print $search_term; ?></em></p>

    <p>To improve your results, you may want to:</p>
    <ul>
      <li>Check that your spelling is correct.</li>
      <li>Remove quotes around phrases to match each word individually: <em>"General Assistance"</em> will match less than <em>General Assistance</em>.</li>
      <li>Consider loosening your query with <em>OR</em>: <em>Final Report</em> will match less than <em>Final OR Report</em>.</li>
    </ul>
  <?php else : ?>
    <p>There are no items to display at this time.</p>
  <?php endif; ?>
<?php endif; ?>
