<?php
 /**
 * @file field__field_file.tpl.php
 * This file is intended to format file fields as part of the file information field collection
 *
 * Available variables:
 *   $file_path = the url to the file itself
 *   $file_ext = the file's extension in lowercase
 *   $file_size = the file's size with appropriate text included (eg 4MB, 18TB)
 *
 * Standard variables used:
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 *
 */

?>

<a class="<?php print $classes; ?>" href="<?php print $file_path;?>">
  <span class="file-type"><?php print $file_ext; ?></span>
  <span class="file-size"><?php print $file_size; ?></span>
</a>