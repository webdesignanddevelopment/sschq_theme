<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 */
?><!DOCTYPE html><?php /* No RDF info required on doctype any more - http://drupal.org/node/1077566#comment-5075420 */ ?>

<!--[if lte IE 7 ]><html class="no-js ie7" lang="en" <?php print $html_attributes . $rdf_namespaces; ?>><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8" lang="en" <?php print $html_attributes . $rdf_namespaces; ?>><![endif]-->
<!--[if (gt IE 8)]><!--><html class="no-js" <?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->

<head>
  <meta charset="utf-8">
  <?php print $head ?>

  <title><?php print $head_title; ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- http://t.co/dKP3o1e -->
  <meta name="HandheldFriendly" content="True">
  <meta name="MobileOptimized" content="320">
  <meta name="viewport" content="width=device-width, target-densitydpi=160dpi, initial-scale=1">

  <!-- For older mobile devices -->
  <link rel="stylesheet" media="handheld" href="/<?php print drupal_get_path('theme', 'magma'); ?>/styles/handheld.css?v=1">
  <!--[if IEMobile 7 ]>
  <meta http-equiv="cleartype" content="on">
  <![endif]-->

  <?php if (!$nojs): ?>
    <!-- HTML5ifer + HTML5 feature detection -->
    <script src="/<?php print drupal_get_path('theme', 'magma'); ?>/scripts/modernizr.custom.min.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/d3.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/c3.js"></script>
  <?php endif; ?>

  <!--[if IE 8 ]>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/es5-shim.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/es5-sham.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/sizzle.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/compat.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/svg.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/aight.js"></script>
    <script src="/<?php print drupal_get_path('theme', 'sschq_theme'); ?>/scripts/d3.ie8.js"></script>
  <![endif]-->

  <!-- For all *real* browsers -->
  <?php if(isset($media_queried_css)){print $media_queried_css;}; ?>

  <!-- For IE < 9 -->
  <?php print $styles; ?>

  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="57x57" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-57x57.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="60x60" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-60x60.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="72x72" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-72x72.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="76x76" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-76x76.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="114x114" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-114x114.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="120x120" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-120x120.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="144x144" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-144x144.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="152x152" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-152x152.png?v=jww04Jn984">
  <link rel="apple-touch-icon" sizes="180x180" href="/sites/all/themes/custom/sschq_theme/images/icons/apple-touch-icon-180x180.png?v=jww04Jn984">
  <link rel="icon" type="image/png" href="/sites/all/themes/custom/sschq_theme/images/icons/favicon-32x32.png?v=jww04Jn984" sizes="32x32">
  <link rel="icon" type="image/png" href="/sites/all/themes/custom/sschq_theme/images/icons/android-chrome-192x192.png?v=jww04Jn984" sizes="192x192">
  <link rel="icon" type="image/png" href="/sites/all/themes/custom/sschq_theme/images/icons/favicon-96x96.png?v=jww04Jn984" sizes="96x96">
  <link rel="icon" type="image/png" href="/sites/all/themes/custom/sschq_theme/images/icons/favicon-16x16.png?v=jww04Jn984" sizes="16x16">
  <link rel="manifest" href="/sites/all/themes/custom/sschq_theme/images/icons/manifest.json?v=jww04Jn984">
  <link rel="shortcut icon" href="/sites/all/themes/custom/sschq_theme/images/icons/favicon.ico?v=jww04Jn984">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="/sites/all/themes/custom/sschq_theme/images/icons/mstile-144x144.png?v=jww04Jn984">
  <meta name="msapplication-config" content="/sites/all/themes/custom/sschq_theme/images/icons/browserconfig.xml?v=jww04Jn984">
  <meta name="theme-color" content="#ffffff">

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>

  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>

  <?php if (!$nojs): ?>
    <?php print $scripts; ?>
  <?php endif; ?>

</body>
</html>
