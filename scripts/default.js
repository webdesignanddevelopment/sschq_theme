/* default.js */
/* Be sure to check what has already been set in Magma.
 *
 * By default the Magma base theme will:
 *  - Add a catch for console.log to avoid errors in <IE9
 *  - Polyfill HTML5 placeholder functionality
 *  - Fixes anchor targetting weirdness across different browsers
 */


/*
 * Kick-off scripts for Shared Service Centre site.
 * (wrapped in the D7 required style.)
 *
 */

(function ($) {
'use strict';
  /**
    * Re-apply fixHeights when ajax requests complete.
    */
  Drupal.behaviors.sscFixHeights = {
    attach: function (context, settings) {
      /* Fix heights */
      Drupal.behaviors.sscFixHeights.trigger();
      var windowWidth = $(window).width();
      var retriggerFix = window.setTimeout( function(){Drupal.behaviors.sscFixHeights.trigger();} , 1000);
  },

    trigger: function(context, settings) {
      //Fix heights
      var windowWidth = $(window).width();
      var fixThese = new Array('.topheight');
      var html = $('div.hidden-div').html();

      for (var i = fixThese.length - 1; i >= 0; i--) {
        jQuery(fixThese[i]).matchHeight({remove:true});
      }
      if (windowWidth < 768) {
        for (var i = fixThese.length - 1; i >= 0; i--) {
          jQuery(fixThese[i]).matchHeight(true);
        }
      }

      checkMobileMenu();

      if (windowWidth > 767) {
        /* Responsive Menu if less than 767px */
        $('#header-extra ul.menu').show();
        $('#header-extra select').remove();
      }
    /* end responsive menu */

  }
};

  Drupal.behaviors.sscMatchHeights = {
    attach: function (context, settings) {
      /* Match heights */

      var fixThese = [
        $('.node .chart > h4'),
        $('.node .chart > .description'),
        $('.node .chart > .c3'),
        $('.node .chart > .c3 > svg'),
      ];

      function reworkHeights(fixThese) {
        $(fixThese).each(function(i) {
          fixThese[i].matchHeight();
        });
      }

      $(document).on("ready resize orientationchange", function() {
        reworkHeights(fixThese);
      });
      $(document).ajaxComplete(function() {
        reworkHeights(fixThese);
      });

    }
  };

  $(document).ready(function(){

    /* Kick Stuff Off */
    initMenu();
    initMenuButton();
    initMenuWatcher();
    initTabSection('#tabbed');
    initMultiTierSelects();
    fixHeights();

    /* Re_init on resize */
    $(window).resize(function(){
      initMenu();
      Drupal.behaviors.sscFixHeights.trigger();
    });

    /* trigger on orientation change */
    if ("onorientationchange" in window) {
      window.addEventListener(orientationEvent, function() {
        initMenu();
      });
    }


    checkMobileMenu();

  });


  /* Checks the height of the body area */
  function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
  }


  /* Trigger a mobile menu when the screen width is small enough */
  function checkMobileMenu() {
    var windowWidth = $(window).width();
    if (windowWidth < 768 && $('#generated_mobile_menu').length < 1) {
      /* Responsive Menu if less than 768px */
      $('#header-extra ul.menu').each(function(){
        var list=$(this),
        select=$(document.createElement('select')).attr('id', 'generated_mobile_menu').insertBefore($(this).hide()).change(function(){
          window.location.href=$(this).val();
        });

        $('>li a', this).each(function(){
          var option=$(document.createElement('option'))
          .appendTo(select)
          .val(this.href)
          .html($(this).html());
          if ($(this).attr('class') === 'selected'){
            option.attr('selected','selected');
          }
        });
        list.hide();
      });
    }
  }

  /* Checks the current window size and determines what settings to apply to the slide-in menu */
  function initMenu(){
    //reset menu height and styles before calc
    $('#slidein-border').addClass('fixed').css('height', 0);
    $('#slidein').css('min-height', '1px').css('height', 'auto');

    var $menu = $('#slidein'),
    winHeight = $(window).height(),
    menuHeight = $menu.outerHeight();

    // Check to see if we can fix the menu in place
    if (winHeight >= menuHeight) {
      $('#slidein-border').addClass('fixed').css('height', 'auto').css('min-height', '100%');
      $menu.css('height', winHeight);
    } else {
      //$('#slidein-border').removeClass('fixed').css('height', menuHeight);
      $('#slidein-border').removeClass('fixed').css('height', getDocHeight());
      $menu.css('height', '100%');
    }
  }

  /* Listens for a click on the menu button and adds/removed the appropriate classes */
  function initMenuButton(){
    $('#menu-button').click(function(event){
      event.stopPropagation();
      window.toggleMenu();
      return false;
    });

    /* also add a click listener on the body to close the menu */
    $('#page-wrapper *:not(#slidein, #slidein *)').click(function(event){
      event.stopPropagation();
      window.toggleMenu('closed');
    });
  }

  /* Watch all links inside the menu and trigger it to open
   * if required, similarly, watch others so we can close the
   * menu if it's not needed. This is specifically for keyboard
   * users tabbing through the page. */
  function initMenuWatcher(){
     $('body a').focus(function(event){
      // Make sure this isn't the menu button first.
      // This was added to stop Firefox keeping the menu open by triggering a focus event
      // on the menu button every time you clicked on it stopping it from toggling and
      // forcing it to stay open.
      if (event.target.id !== 'menu-button') {
         if ($(event.target).parents('#slidein').length > 0){
          window.toggleMenu('open');
        } else {
          window.toggleMenu('closed');
        }
      }
    });


    /* Also hide the menu if someone starts scrolling. Add some sensible
     * limits in here to stop it always hiding */
    // console.log($('#slidein-border').hasClass('fixed'));
    if ($('#slidein-border').hasClass('fixed')) {
      var prevScroll = 0,
          scrollDistanceTrigger = 300,
          scrollTimeout = 200;

      $(document).scroll(function(event){
        var thisScroll = {
          timestamp: event.timeStamp,
          distance: window.scrollY
        };

        /* reset prevScroll if it's too old or doesn't exist */
        if (!prevScroll.timestamp || (thisScroll.timestamp - prevScroll.timestamp > scrollTimeout)){
          prevScroll = thisScroll;
          return;
        }

        /* test for scroll distance and trigger if long enough */
        if (Math.abs(thisScroll.distance - prevScroll.distance) > scrollDistanceTrigger){
          window.toggleMenu('closed');
        }

      });
    }
  }

   /* Toggle the state of the menu. This is accessible from outside this file
   with:
    window.toggleMenu('open'); // open it
   or
    window.toggleMenu('closed'); // close it
   or
    window.toggleMenu(); // toggle it

  The function is wrapped in debouce which should stop it from being called serveral
  times on a single click or touch event. This is mainly an issue in Firefox that was
  not opening the menu correctly in mobile or desktop.
   */
   window.toggleMenu = debounce(function(state){
     var $menu = $('#slidein');
     if (typeof state === 'undefined'){
        if ($menu.hasClass('open')){
          state = 'close';
        } else {
          state = 'open';
        }
     }

     if (state == "closed" || state == "close"){
       $menu.removeClass('open');
     } else {
       $menu.addClass('open');
     }
   }, 250, true);

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  function debounce(func, wait, immediate) {
  var timeout;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timeout);
      //trigger immediate function first then apply timer if immediate is true
      if (immediate && !timeout) func.apply(context, args);
      timeout = setTimeout(function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      }, wait);
    }
  }

  /* Tabbed section controls and config */
  function initTabSection(selector){
    var $tabsection = $(selector),
    $tabs = $(selector+' .block'),
    tabcount = $tabs.length,
    tabHeadings = {},
    tabMenu = '';

    if ($tabsection && tabcount > 1){
    //we have tabs!
      $tabsection.addClass('has-tabs');
      $.each($tabs, function(index, tab){
        //get the heading
        tab = $(tab);
        tabHeadings[index] = {
          'title': tab.find('h2').html(),
          'id' : tab.attr('id')
        };

        //style it as a tab
        tab.addClass('tab');

        //don't hide the first one.
        if (index !== 0){
          tab.hide();
        }

      });


      /* create the tab menu */
      tabMenu = "<ul class='tab-menu'>";
      for (var i in tabHeadings) {
        tabMenu += "<li><a ";
        if (i == 0){
          tabMenu += "class='active'";
        }
        tabMenu += "href='#"+tabHeadings[i].id+"'>"+tabHeadings[i].title+"</a></li>";
      }
      tabMenu += "</ul>";
      $tabsection.before(tabMenu);

      // set up a listener to act on menu clicks
      $('.tab-menu a').click(function(e){
        $('.tab-menu a').removeClass('active');
        e.preventDefault();
        showTab($(this).attr('href'));
        $(this).addClass('active');
      });
    }
  }

  /* Toggle the tabs */
  function showTab(tabToShow){
    if ($(tabToShow+':visible').length == 0){
      $('.tab:visible').fadeOut(300, function(){
        $(tabToShow).fadeIn(300);
      });
    }
  }

  /* fixHeights will set all divs of the specified kind set to the same height
   * which is calculated by testing all of them on the page and picking the
   * tallest one. */
   function fixHeights(){
    var selectorsToFix = [
      '.search-result'],
      winWidth = $(window).width();

    for (var i = selectorsToFix.length - 1; i >= 0; i--) {
      var $items = $(selectorsToFix[i]);
      //Bail if there's nothing to fix
      if ($items.length === 0){
        return;
      }
      //reset any heights that are already set
      $items.css('height', 'auto');

      //if we're wide enough to have columns, do stuff
      if (winWidth > 480){
        var columns = 2; /* 2 columns for 480px+ layouts */
        if (winWidth > 768){
          columns = 3; /* 3 cols for 768+ */
        }
        //check heights per row
        for (var i = 0; i < $items.length; i = i+columns){
          var ulMaxHeight = 0;
          for (var j=i; (j-i) < columns; j++){
            var thisHeight = $items.eq(j).outerHeight();
            if (thisHeight > ulMaxHeight){
              ulMaxHeight = thisHeight;
            }
          }
          //set heights of the items in this set.
          $items.slice(i, j).css('height', ulMaxHeight);
        }
      }
    };
  }

  /* The following code relates to initMultiTierSelects */
  function hideOptionFromSelect(select_selector, option_selector){
    option_selector.appendTo($(select_selector + "_hidden"));
  }

  function addOptionToSelect(select_selector, option_selector){
    // Adds option to select, order should be maintained without the need to sort
    var option_text = $(option_selector).text();
    var child_options = $(select_selector).children();
    $(option_selector).insertAfter($(select_selector).children().last());
  }

  function hideSelectOptionsByValuePrefix(select_selector, prefix_exception){
    // Hides Select Options unless the option value prefix matches argument (string) prefix_exception
    var children = $(select_selector).children();

    // Iterate the select list to hide unmatched options
    for (var i = children.length - 1; i >= 0; i-- ) {
      var hide_element = $(children[i]).val().lastIndexOf(prefix_exception, 0) !== 0;
      if (hide_element && $(children[i]).text() !== "- None -"){
        hideOptionFromSelect(select_selector, $(children[i]))
      }
    }

    // iterate hidden select list to re-add previously hidden entries which do match
    var hidden_children = $(select_selector + "_hidden").children();
    for (var i = hidden_children.length - 1; i >= 0; i-- ) {
      var show_element = $(hidden_children[i]).val().lastIndexOf(prefix_exception, 0) === 0;
      if (show_element){
        addOptionToSelect(select_selector, hidden_children[i]);
      }
    }
  }

  function initMultiTierSelects(){
    /*
     * Sets up tiered selects where the options for the first select will set the elements of the second
     * This is achieved by Select2 option values being prefixed with X_ where X is the value of the
     * top level select.
     * Used on http://ssc.test.ha.gbuild.net/contact and http://ssc.test.ha.gbuild.net/contact
     */

    if ($("#edit-submitted-service-line").length) {
      $("#edit-submitted-select-service").attr("disabled", "disabled");

      var hiddenSelectList = "<select id=\"edit-submitted-select-service_hidden\" class=\"hidden\" disabled></select>";
      $(hiddenSelectList).insertAfter("#edit-submitted-select-service");

      $("#edit-submitted-service-line").change(function(){
        var tier_2_prefix = $("#edit-submitted-service-line").val() + "_";
        if (tier_2_prefix === "_"){
          $("#edit-submitted-select-service").attr("disabled", "disabled");
        } else {
          $("#edit-submitted-select-service").removeAttr("disabled");
          hideSelectOptionsByValuePrefix("#edit-submitted-select-service", tier_2_prefix);
        }
      });
    }
  }

  (function() {
      var fontSize = $.cookie('fontSize'),
          $body;
          if (fontSize !== null) {
            $('body').css('font-size', fontSize + 'px');
          }
          $('.js-print').on('click', function (evt) {
            evt.preventDefault();
            window.print();
          });
    }());
})(jQuery);
