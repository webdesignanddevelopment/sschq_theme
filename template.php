<?php

/**
 * Implement hook_preprocess_html().
 */
function sschq_theme_preprocess_html(&$vars) {
  
  //
  // Redirect all traffic from /decomissioned to /decommissioned.
  //
  global $base_path;
  
  $url_components       = explode('/', request_uri());
  $redirect_destination = '';
  
  $redirect_keywords = ['decomissioned', 'decommission', 'decomission'];
  
  foreach ($redirect_keywords as $redirect_keyword) {
    if($url_components[1] === $redirect_keyword) {
      if(isset($url_components[2])) {
        $redirect_destination = 'decommissioned/' . $url_components[2];
      }
    }
  }
  
  if (isset($redirect_destination) && !empty($redirect_destination)) {
    header('Location: ' . $base_path . $redirect_destination);
  }
  
  //provide css for the glue-eaters.

  css_320_and_up($vars);
}

/**
 * Implement hook_preprocess_node().
 */
function sschq_theme_preprocess_node(&$vars) {
  /* display a notice if this is dummy content */
  if ($dummy_field = field_get_items('node', $vars['node'], 'field_dummy_version')){
    $vars['dummy'] = $dummy_field[0]['value'];
  } else {
    $vars['dummy'] = false;
  }

  if (isset($vars['node'])){
    switch ($vars['node']->type) {
      case 'person':
        $vars['person'] = create_person_block($vars['node']);
        break;
      case 'blog_post':
        $author_nid = field_get_items('node', $vars['node'], 'field_author');
        if ($author_nid){
          $vars['author_img'] = create_person_photo_block($author_nid[0]['node']);
        }
        break;
      case 'download':
        $vars['classes_array'][] = 'has-downloads';
      default:
        $vars['person'] = false;
        break;
    }
  }
}

function sschq_theme_preprocess_field(&$vars) {

  switch ($vars['element']['#field_name']) {
    case 'field_video_reference':
      // Handle video references using the deewr video block functionality - not used on teasers.
      if (module_exists('deewr_video_block')){
        if ($vars['element']['#view_mode'] !== 'teaser') {
          $data = array(
            'video_node' => $vars['element'][0]['#markup'],
            'video_field' => 'field_media_id',
            );

          $block = _quick_render_deewr_video($data);

          /* replace the id with the video markup */
          $vars['items'][0]['#markup'] = $block['content']['#markup'];
        }
      }
    break;
    case 'field_file':
      /* pull out the variables we need for easy access in the template */
      if (isset($vars['element']['#items'][0]) && $file = $vars['element']['#items'][0]){
        $vars['file_path'] = file_create_url($file['uri']);
        $vars['file_size'] = file_size($file['filesize']);
        $vars['file_ext'] = strtolower(pathinfo($file['filename'], PATHINFO_EXTENSION));
        $vars['classes_array'][] = 'file-type-'.$vars['file_ext'];
      }
    default:
    case 'field_chart_id':
      /* generate some custom markup for the chart_id field */
      /* Test if we want this to have a link based on user permission */
      $vars['show_link'] = user_access('administer nodes');

      $vars['link_path'] = url('node/'.$vars['element']['#object']->nid);

    default:
      # code...
      break;
  }

}

/**
 * Implement hook_preprocess_page().
 */
function sschq_theme_preprocess_page(&$vars) {

  global $user;

  /* Modify the title for personalised contact forms
   * (Node 137 belongs to the personal contact form) */
  if (isset($_GET['pid']) && $vars['node']->nid == '137' && $p_node = node_load($_GET['pid'])){
    $vars['title'] = "Contact $p_node->title";
  }

  if (isset($vars['node']->type)) {
      $vars['theme_hook_suggestions'][]='page__'.$vars['node']->type;
  }

}

/*
 * Modify the default search block
 */
function sschq_theme_form_alter(&$form, &$form_state, $form_id) {
  //var_dump($form_state);

  switch ($form_id) {
     // Sidebar search box
     case 'search_block_form':
      $form['search_block_form']['#title'] = t('Search terms:'); // Change the text on the label element
      $form['search_block_form']['#title_display'] = 'invisible'; // Toggle label visibilty
      $form['search_block_form']['#size'] = 20;  // define size of the textfield
      //$form['search_block_form']['#default_value'] = t('Ask your question'); // Set a default value for the textfield
      $form['search_block_form']['#attributes']['title'] = t('Enter some words describing what you want to find'); // Set a default value for the title (for tooltips)
      $form['actions']['submit']['#value'] = t('Search'); // Change the text on the submit button
      // HTML5 placeholder attribute instead of using javascript
      $form['search_block_form']['#attributes']['placeholder'] = t('Search the SSC website');
    break;

    //custom search pages
    case 'apachesolr_search_custom_page_search_form':
      if (isset($form['#search_page']['page_id'])){
        /* Nothing here for now. Just the one search page */
      }
    break;

    case 'webform_client_form_137':
      /* Customise the form with the Person's name */
      foreach ($form_state['webform']['component_tree']['children'] as $key => $field) {
        if ($field['form_key'] == 'intro'){
          $pid = (isset($_GET['pid'])) ?  $_GET['pid'] : false;
          if ($pid && $p_node = node_load($pid)){
            $link_markup = l($p_node->title, 'node/'.$pid);

            //Change the intro text on the form
            $form['submitted']['intro']['#markup'] = "<p class='summary'>Complete this form to send a quick enquiry to $link_markup.</p>";

            // Change the email address on submission
            array_unshift($form['#submit'],'ssc_theme_webform_form_submit');
          }
        }
      }
    break;

    //let the default search page and any others fall through to the default:
    default:
      $form['basic']['submit']['#value'] = t('Search');
      $form['basic']['keys']['#title'] = t('Search terms');
      $form['basic']['keys']['#attributes']['placeholder'] = t('Enter some words describing what you want to find');
      break;
  }
}

/* To set the default "Select an option" to the exposed form It must be associated with the SSChq dashboard view settings */
function sschq_theme_form_views_exposed_form_alter(&$form, &$form_state) {
  if (isset($form_state['view']) && ($form_state['view']->name === "dashboard_v2")) {
    if(isset($form) && ($form['#id'] === "views-exposed-form-dashboard-v2-page")) {
      if(!isset($form_state['view']->exposed_input['field_reporting_requirements_tid'])) {
        $form_state['input']['field_reporting_requirements_tid'] = "All";
      }
    }
  }
}

/* Implements template_preprocess_search_result();
 * Sends additional data to search_result.tpl.php */
function sschq_theme_preprocess_search_result(&$vars) {
  $node = $vars['result']['node'];
  $info = &$vars['info_split'];

  /* kpr($info); */
  /* depending on how solr is feeling, the node type can be found in a few places */
  if (isset($node->type)){
    $bundle = $node->type;
  } else if (isset($vars['result']['bundle'])){
    $bundle = $vars['result']['bundle'];
  }

  if (isset($bundle)){
    $vars['classes_array'][] = $bundle;
  }

  /* TODO: do checks to get the date created value for docs and blogs when solr returns fields consistently */
}

/* Transforms the staff_id value in to an email address that'll be
 * used to actually send the request.
 * -- Only used for the person contact form -- */
function sschq_theme_webform_form_submit(&$form, &$form_state){

  $staff_id = $form['submitted']['staff_id']['#value'];
  $staff_node = node_load($staff_id);

  // Test that staff_id had a valid staff member and it has an email field.
  if (isset($staff_node) && gettype($staff_node) == 'object' && $field_email = field_get_items('node', $staff_node, 'field_email')) {
    // We have a node, and an email field. Use that to send the email.
    $webform_email_info = &$form['#node']->webform['emails'];
    // In case we're sending many different versions of this for
    // to different places, check that we're replacing the right one.
    foreach ($webform_email_info as $key => &$email_instructions) {
      // if the value of the email field isn't an email address, swap
      // it for the email value from the staff node.
      if (!stristr($email_instructions['email'], '@')){
        $email_instructions['email'] = $field_email[0]['value'];
      }
    }
  }
}

/*
 * Implements template_preprocess_block()
 */
function sschq_theme_preprocess_block(&$vars) {
  switch ($vars['block_html_id']) {
    case 'block-search-form':
      $vars['classes_array'][] = 'clearfix';
      break;
  }
}

/**
 * Implement theme_apachesolr_search_noresults().
 * ONLY works if a search term has been provided and apachesolr search is being used.
 */
function sschq_theme_apachesolr_search_noresults() {
  //Get the search terms and store them for possible use in the template
  $node = menu_get_item();
  $search_page = (isset($node['page_arguments'][0])? apachesolr_search_page_load($node['page_arguments'][0]): null);
  $path_args = arg();
  $current_uri = $_SERVER['REQUEST_URI'];
  // Test to see if a query has been supplied
  $query_test = explode('/'.$search_page['search_path'].'/', $current_uri);
  /* If there's nothing but the search path, $query_test will return an array with two empty values.
   * If there's a query on the end of the uri, the item at index 1 will contain the query. */
  $has_query = (empty($query_test[0]) && !empty($query_test[1]));
  // Get the search terms (only returns anything useful if terms are provided)
  $search_term = str_replace('/'.$search_page['search_path'].'/', '', $current_uri);
  /* not sure where this is getting double-encoded, but it needs to be double decoded to work in all scenarios. */
  $search_term = urldecode(urldecode($search_term));

  if ($has_query){
    $markup = t('<h2>No results found</h2>');
    $markup .= t('<p>We couldn\'t find any content that might interest you based on your term');
    $markup .= (stristr($search_term, ' ')?'s': '');
    $markup .= t(' <em>%search_term</em>.</p>', array('%search_term'=>$search_term));
  } else {
    /* The index is empty, no terms have been entered or something has gone wrong */
    $markup = t('<h2>What would you like to search for?</h2>');
    $markup .= t('<p>Enter some terms in the search box to search the site.</p>');
  }
  /* Add generic help */
  $markup .= t('<p>To improve your suggestions you may want to:</p>
      <ul>
        <li>Check if your spelling is correct.</li>
        <li>Remove quotes around phrases to match each word individually: <em>"General Assistance"</em> will match less than <em>General Assistance</em>.</li>
        <li>Consider loosening your query with <em>OR</em>: <em>Financial Services</em> will match less than <em>Financial OR Services</em>.</li>');
  $markup .= t('</ul>');
  return $markup;
}

/**************************/
/* HELPER FUNCTIONS BELOW */
/**************************/


/*
 * We're using media queries to handle progressive enhancement of our templates.
 *
 * Because IE 6-8 doesn't handle media queries (and the current JavaScript solutions create other issues),
 * let's insert a conditional comment for IE < 9 to display all or media-queried stylesheets. IE < 9 won't
 * load the media query stylesheets, and other browsers won't load these ones.
 *
 * Note: this is a little more complicated than it possibly should be, because Drupal doesn't let us use the
 *       same stylesheet twice.
 */
function css_320_and_up(&$vars) {

  $original_css = drupal_add_css();
  $media_queried_css = drupal_get_css();
  $vars['media_queried_css'] = $media_queried_css;  // Store the current CSS in a variable for later use.
  drupal_static_reset('drupal_add_css');            // Reset the CSS Drupal has stored, so we can add it in again below.

  // For each appropriate stylesheet, duplicate for < IE9 using conditional comments.
  foreach ($original_css as $path => $attributes) {
    if ($attributes['group'] == CSS_THEME && preg_match('/^(all|screen|print|handheld)$/i', $attributes['media']) == 0) {

      // Window 7 Phone gets the 320px stylesheets only.
      $ie_browsers = (preg_match('/min-width: 320px/i', $attributes['media']) !== 0) ? 'lt IE 9' : '(lt IE 9)&(!IEMobile)';

      drupal_add_css($path, array(
        'browsers'    => array('IE' => $ie_browsers, '!IE' => FALSE),
        'data'        => $attributes['data'],
        'group'       => CSS_THEME,
        'media'       => 'screen',
        'preprocess'  => $attributes['preprocess'],
        'weight'      => $attributes['weight'],
      ));
    }
  }
}


/* Create markup for People blocks.
 * Allows consistent code and layout across the entire site.
 *
 * *** NOTE: CHANGES MADE HERE SHOULD ALSO BE MADE IN ***
 * *** views-view-field--block--nothing.tpl.php      ***
 * *** for complete consistency across the site      ***/

/* $node should be the $node object of the person you are trying to create a block for. */

function create_person_block($node) {
  /* quick test so we don't waste time */
  if ($node->type !== 'person') {return;}

  $referrer_nid = menu_get_item();
  $referrer_nid = $referrer_nid['page_arguments'][0]->nid;

  /* define some variables with defaults */
  $link = url('node/'.$node->nid);
  $name = $node->title;
  $initials = get_initials($name);
  $job_title = ssc_get_field_value($node, 'field_job_title', false, false);
  $phone = ssc_get_field_value($node, 'field_phone', '02 6123 4444 (temp)', false);
  $location = ssc_get_field_value($node, 'field_location', '<p>60 Marcus Clarke St<br />Canberra ACT, Australia (temp)</p>', false);
  $email = ssc_get_field_value($node, 'field_email', 'info@ssc.gov.au', false);
  $ob_email = hide_email($email);
  $person_type = ssc_get_field_value($node, 'field_person_type', 'unknown', true);
  $image = create_person_photo_block($node);
  $description = ssc_get_field_value($node, 'field_short_decription',false, false);

  /* Do some rendering! */
  $output = "
  <div class='person-profile person-$node->nid clearfix'>
    <div class='profile-intro'>
      $image
      <div class='profile-name'>
          $name";
  if ($job_title){
    $output .= "<span class='profile-title'>$job_title</span>";
  }
  $output .= "
      </div><!-- / .profile-name -->
    </div><!-- / .profile-intro -->
    <div class='person-details'>
      <div class='phone'>
        <strong>Phone</strong>
        $phone
      </div>
      <div class='location'>
      <strong>Location</strong>
        $location
      </div>
      <div class='email'>
      <strong>Contact $name</strong>
        <a href='/node/137?pid=$node->nid&rid=$referrer_nid' class='button message'>Send $name a message</a>
      </div>
    </div>
  </div>";
  /* send the finished html back */
  return $output;
}


/* Used to just get the profile photo or generated initials-in-a-box
 * profile image replacement. */
function create_person_photo_block($node) {
  $person_type = ssc_get_field_value($node, 'field_person_type', 'unknown', true);
  $profile_photo = ssc_get_field_value($node, 'field_profile_photo', false, false);
  $initials = get_initials($node->title);
  $image_style = 'profile_image';

  if ($profile_photo){
    /* theme up the image */
    $img_item = array(
       'item' => array(
         'alt' => addslashes("Photo of "+$node->title),
         'title' => addslashes($node->title),
         'uri' => $profile_photo["uri"],
         'attributes' => array('class' => 'profile-photo circle')
       ),
      'image_style' => $image_style
    );
    $image = theme_image_formatter($img_item);
  } else {
    /* create some html to hold their initials */
    $image = "<div class='profile-photo has-initials '><div class='circle $person_type'><span class='initials'>$initials</span></div></div>";
  }

  return $image;

}

/* Field value getter helper
 *
 * Note: Only ever returns the first item so it's no good for multiple values.
 *
 * $node - object - the Node object we're working with now
 * $field_name - string - a string of the field we want to get the value out of
 * $default_value - any - what to send back if the field is empty.
 * $raw - boolean - should we try and get the raw value of this field?
 **/
function ssc_get_field_value($node, $field_name, $default_value, $raw){
  if (isset($node->$field_name) && !empty($node->$field_name)){
    $field_name_field = field_get_items('node', $node, $field_name);
    if ($raw) {
      $field_value = $field_name_field[0]['value'];
    } else {
      $field_value = field_view_value('node', $node, $field_name, $field_name_field[0]);
      // only send back a known value, or send the whole field object
      if (isset($field_value['#markup'])){
        $field_value = $field_value['#markup'];
      } else if (isset($field_value['#item'])){
        $field_value = $field_value['#item'];
      }
    }
  } else {
    $field_value = $default_value;
  }
  return $field_value;
}


/* Get the initials based on a string - basically just grabs the first letter
 * of each word in a string and sends them back.
 *
 *  e.g. ssc_theme_get_initials("Dave Thomas");
 *  // returns "DT"
 **/

function get_initials($name){
  $name_array = explode(" ", $name);
  $initials = "";
  foreach ($name_array as $n) {
    $initials .= $n[0];
  }
  return $initials;
}

/* Take a string (normally an email) and change it in to a
 * buch of character codes to avoid bots identifying it as an email
 * address as a form of spam protection
 **/
function hide_email($email) {
  $ob_email = '';
  for($i = 0; $i < strlen($email); $i++) {
    $ob_email .= '&#' . ord($email[$i]) . ';';
  }
  return $ob_email;
}

/* generate a nice file size based on the bytes on the file field. */
function file_size($size)
{
    $filesizename = array("&nbsp;Bytes", "&nbsp;KB", "&nbsp;MB", " GB", " TB", " PB", " EB", " ZB", " YB");
    return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $filesizename[$i] : '0 Bytes';
}


/* Helper function for getting video posters */
/* Just gets and returns the poster url when provided with a video page ID */
/* returns - a string version of the poster path */

function get_poster_url($video_page_id) {
  /* if deewrmedia exists, continue on - otherwise, do nothing */
  if (module_exists('deewrmedia')){

    /* load the video node */
    $node = node_load($video_page_id);

    $video_feature_img =

    $sourceid = $video_id[0]['sourceid'];

    /* get video json */
    $video = deewrmedia_fetch_player($sourceid);

    /* get image */
    return isset($video['poster']) ? $video['poster']: false; /* maybe replace it with a default? */

  }
}
